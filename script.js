const tabs = document.querySelectorAll('.tabs-title');
const contents = document.querySelectorAll('.content');

for(let i = 0; i < tabs.length; i++) {
    tabs[i].addEventListener('click', (event) => {
        let tabsActual = event.target.parentElement.children;
        for(let a = 0; a < tabsActual.length; a++) {
            tabsActual[a].classList.remove('active');
        }

        event.target.classList.add('active');

        let contentsActual = event.target.parentElement.nextElementSibling.children;
        for(let b = 0; b < contentsActual.length; b++) {
            contentsActual[b].classList.remove('content-active');
        }

        contents[i].classList.add('content-active');
    })
}
